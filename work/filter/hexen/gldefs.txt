object QXPlayer
{
	frame PLY5A { light ZOMBIEATK }
	frame PLY5I { light ZOMBIEATK }
	frame PLY5C { light ZOMBIEATK }
}

object QXPlayer2
{
	frame PL24H { light ZOMBIEATK }
}

object QXPlayer3
{
	frame PL24K { light ZOMBIEATK }
}

pointlight QBOX
{
	color 0.0 0.0 0.0
	size 0
}

object QExplosiveBox
{
	frame QBOXA { light QBOX }
	frame QBOXB { light QBOX }
}

pointlight QMINFX1
{
	color 0.5 0.25 0.0
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QMINFX1_X0
{
	color 1.0 0.5 0.0
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QMINFX1_X1
{
	color 0.9 0.45 0.0
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QMINFX1_X2
{
	color 0.8 0.4 0.0
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QMINFX1_X3
{
	color 0.7 0.35 0.0
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QMINFX1_X4
{
	color 0.6 0.3 0.0
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QMINFX1_X5
{
	color 0.5 0.25 0.0
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QMINFX1_X6
{
	color 0.4 0.2 0.0
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QMINFX1_X7
{
	color 0.3 0.15 0.0
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QMINFX1_X8
{
	color 0.2 0.1 0.0
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QMINFX1_X9
{
	color 0.1 0.05 0.0
	size 72
	//secondarySize 76
	//interval 1.001
}

object QMinotaurFX1
{
	frame FX12 { light QMINFX1 }
	frame XXXXA { light QMINFX1_X0 }
	frame XXXXB { light QMINFX1_X1 }
	frame XXXXC { light QMINFX1_X2 }
	frame XXXXD { light QMINFX1_X3 }
	frame XXXXE { light QMINFX1_X4 }
	frame XXXXF { light QMINFX1_X5 }
	frame XXXXG { light QMINFX1_X6 }
	frame XXXXH { light QMINFX1_X7 }
	frame XXXXI { light QMINFX1_X8 }
	frame XXXXJ { light QMINFX1_X9 }
}