object QDPlayer
{
	frame PLY5A { light ZOMBIEATK }
	frame PLY5I { light ZOMBIEATK }
	frame PLY5C { light ZOMBIEATK }
}

object QDPlayer2
{
	frame PL24H { light ZOMBIEATK }
}

object QDPlayer3
{
	frame PL24K { light ZOMBIEATK }
}

object QZombieMan
{
	frame POSSF { light ZOMBIEATK }
}

object QStealthZombieMan
{
	frame POSSF { light ZOMBIEATK }
}

object QShotgunGuy
{
	frame SPOSF { light ZOMBIEATK }
}

object QStealthShotgunGuy
{
	frame SPOSF { light ZOMBIEATK }
}

object QChaingunGuy
{
	frame CPOSF { light ZOMBIEATK }
}

object QStealthChaingunGuy
{
	frame CPOSF { light ZOMBIEATK }
}

object QWolfensteinSS
{
	frame SSWVG { light ZOMBIEATK }
}

object QCyberdemon
{
	frame CYBRF { light ZOMBIEATK }
}

object QSpiderMastermind
{
	frame SPIDH { light ZOMBIEATK }
}

pointlight QPLASMA
{
	color 0.25 0.25 0.5
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QPLASMA_X0
{
	color 0.5 0.5 1.0
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QPLASMA_X1
{
	color 0.45 0.45 0.9
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QPLASMA_X2
{
	color 0.4 0.4 0.8
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QPLASMA_X3
{
	color 0.35 0.35 0.7
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QPLASMA_X4
{
	color 0.3 0.3 0.6
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QPLASMA_X5
{
	color 0.25 0.25 0.5
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QPLASMA_X6
{
	color 0.2 0.2 0.4
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QPLASMA_X7
{
	color 0.15 0.15 0.3
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QPLASMA_X8
{
	color 0.1 0.1 0.2
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QPLASMA_X9
{
	color 0.05 0.05 0.1
	size 72
	//secondarySize 76
	//interval 1.001
}

object QPlasmaBall
{
	frame PLSS { light QPLASMA }
	frame XXXXA { light QPLASMA_X0 }
	frame XXXXB { light QPLASMA_X1 }
	frame XXXXC { light QPLASMA_X2 }
	frame XXXXD { light QPLASMA_X3 }
	frame XXXXE { light QPLASMA_X4 }
	frame XXXXF { light QPLASMA_X5 }
	frame XXXXG { light QPLASMA_X6 }
	frame XXXXH { light QPLASMA_X7 }
	frame XXXXI { light QPLASMA_X8 }
	frame XXXXJ { light QPLASMA_X9 }
}

pointlight QBFG
{
	color 0.0 0.5 0.0
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QBFG_X0
{
	color 0.0 1.0 0.0
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QBFG_X1
{
	color 0.0 0.9 0.0
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QBFG_X2
{
	color 0.0 0.8 0.0
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QBFG_X3
{
	color 0.0 0.7 0.0
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QBFG_X4
{
	color 0.0 0.6 0.0
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QBFG_X5
{
	color 0.0 0.5 0.0
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QBFG_X6
{
	color 0.0 0.4 0.0
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QBFG_X7
{
	color 0.0 0.3 0.0
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QBFG_X8
{
	color 0.0 0.2 0.0
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QBFG_X9
{
	color 0.0 0.1 0.0
	size 72
	//secondarySize 76
	//interval 1.001
}

object QBFGBall
{
	frame BFS1 { light QBFG }
	frame XXXXA { light QBFG_X0 }
	frame XXXXB { light QBFG_X1 }
	frame XXXXC { light QBFG_X2 }
	frame XXXXD { light QBFG_X3 }
	frame XXXXE { light QBFG_X4 }
	frame XXXXF { light QBFG_X5 }
	frame XXXXG { light QBFG_X6 }
	frame XXXXH { light QBFG_X7 }
	frame XXXXI { light QBFG_X8 }
	frame XXXXJ { light QBFG_X9 }
}

object QBFGExtra
{
	frame XXXXA { light QBFG_X0 }
	frame XXXXB { light QBFG_X1 }
	frame XXXXC { light QBFG_X2 }
	frame XXXXD { light QBFG_X3 }
	frame XXXXE { light QBFG_X4 }
	frame XXXXF { light QBFG_X5 }
	frame XXXXG { light QBFG_X6 }
	frame XXXXH { light QBFG_X7 }
	frame XXXXI { light QBFG_X8 }
	frame XXXXJ { light QBFG_X9 }
}

pointlight QAPLASMA
{
	color 0.25 0.5 0.0
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QAPLASMA_X0
{
	color 0.5 1.0 0.0
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QAPLASMA_X1
{
	color 0.45 0.9 0.0
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QAPLASMA_X2
{
	color 0.4 0.8 0.0
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QAPLASMA_X3
{
	color 0.35 0.7 0.0
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QAPLASMA_X4
{
	color 0.3 0.6 0.0
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QAPLASMA_X5
{
	color 0.25 0.5 0.0
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QAPLASMA_X6
{
	color 0.2 0.4 0.0
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QAPLASMA_X7
{
	color 0.15 0.3 0.0
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QAPLASMA_X8
{
	color 0.1 0.2 0.0
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QAPLASMA_X9
{
	color 0.05 0.1 0.0
	size 72
	//secondarySize 76
	//interval 1.001
}

object QArachnotronPlasma
{
	frame APLS { light QAPLASMA }
	frame XXXXA { light QAPLASMA_X0 }
	frame XXXXB { light QAPLASMA_X1 }
	frame XXXXC { light QAPLASMA_X2 }
	frame XXXXD { light QAPLASMA_X3 }
	frame XXXXE { light QAPLASMA_X4 }
	frame XXXXF { light QAPLASMA_X5 }
	frame XXXXG { light QAPLASMA_X6 }
	frame XXXXH { light QAPLASMA_X7 }
	frame XXXXI { light QAPLASMA_X8 }
	frame XXXXJ { light QAPLASMA_X9 }
}

pointlight QBARBAL
{
	color 0.25 0.5 0.25
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QBARBAL_X0
{
	color 0.5 1.0 0.5
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QBARBAL_X1
{
	color 0.45 0.9 0.45
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QBARBAL_X2
{
	color 0.4 0.8 0.4
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QBARBAL_X3
{
	color 0.35 0.7 0.35
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QBARBAL_X4
{
	color 0.3 0.6 0.3
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QBARBAL_X5
{
	color 0.25 0.5 0.25
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QBARBAL_X6
{
	color 0.2 0.4 0.2
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QBARBAL_X7
{
	color 0.15 0.3 0.15
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QBARBAL_X8
{
	color 0.1 0.2 0.1
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QBARBAL_X9
{
	color 0.05 0.1 0.05
	size 72
	//secondarySize 76
	//interval 1.001
}

object QBaronBall
{
	frame BAL7 { light QBARBAL }
	frame XXXXA { light QBARBAL_X0 }
	frame XXXXB { light QBARBAL_X1 }
	frame XXXXC { light QBARBAL_X2 }
	frame XXXXD { light QBARBAL_X3 }
	frame XXXXE { light QBARBAL_X4 }
	frame XXXXF { light QBARBAL_X5 }
	frame XXXXG { light QBARBAL_X6 }
	frame XXXXH { light QBARBAL_X7 }
	frame XXXXI { light QBARBAL_X8 }
	frame XXXXJ { light QBARBAL_X9 }
}

pointlight QCACOBAL
{
	color 0.5 0.0 0.25
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QCACOBAL_X0
{
	color 1.0 0.0 0.5
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QCACOBAL_X1
{
	color 0.9 0.0 0.45
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QCACOBAL_X2
{
	color 0.8 0.0 0.4
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QCACOBAL_X3
{
	color 0.7 0.0 0.35
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QCACOBAL_X4
{
	color 0.6 0.0 0.3
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QCACOBAL_X5
{
	color 0.5 0.0 0.25
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QCACOBAL_X6
{
	color 0.4 0.0 0.2
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QCACOBAL_X7
{
	color 0.3 0.0 0.15
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QCACOBAL_X8
{
	color 0.2 0.0 0.1
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QCACOBAL_X9
{
	color 0.1 0.0 0.05
	size 72
	//secondarySize 76
	//interval 1.001
}

object QCacodemonBall
{
	frame BAL7 { light QCACOBAL }
	frame XXXXA { light QCACOBAL_X0 }
	frame XXXXB { light QCACOBAL_X1 }
	frame XXXXC { light QCACOBAL_X2 }
	frame XXXXD { light QCACOBAL_X3 }
	frame XXXXE { light QCACOBAL_X4 }
	frame XXXXF { light QCACOBAL_X5 }
	frame XXXXG { light QCACOBAL_X6 }
	frame XXXXH { light QCACOBAL_X7 }
	frame XXXXI { light QCACOBAL_X8 }
	frame XXXXJ { light QCACOBAL_X9 }
}

pointlight QIMPBAL
{
	color 0.5 0.25 0.0
	size 28
	//secondarySize 32
	//interval 1.001
}

pointlight QIMPBAL_X0
{
	color 1.0 0.5 0.0
	size 36
	//secondarySize 40
	//interval 1.001
}

pointlight QIMPBAL_X1
{
	color 0.9 0.45 0.0
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QIMPBAL_X2
{
	color 0.8 0.4 0.0
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QIMPBAL_X3
{
	color 0.7 0.35 0.0
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QIMPBAL_X4
{
	color 0.6 0.3 0.0
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QIMPBAL_X5
{
	color 0.5 0.25 0.0
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QIMPBAL_X6
{
	color 0.4 0.2 0.0
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QIMPBAL_X7
{
	color 0.3 0.15 0.0
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QIMPBAL_X8
{
	color 0.2 0.1 0.0
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QIMPBAL_X9
{
	color 0.1 0.05 0.0
	size 72
	//secondarySize 76
	//interval 1.001
}

object QDoomImpBall
{
	frame BAL1 { light QIMPBAL }
	frame XXXXA { light QIMPBAL_X0 }
	frame XXXXB { light QIMPBAL_X1 }
	frame XXXXC { light QIMPBAL_X2 }
	frame XXXXD { light QIMPBAL_X3 }
	frame XXXXE { light QIMPBAL_X4 }
	frame XXXXF { light QIMPBAL_X5 }
	frame XXXXG { light QIMPBAL_X6 }
	frame XXXXH { light QIMPBAL_X7 }
	frame XXXXI { light QIMPBAL_X8 }
	frame XXXXJ { light QIMPBAL_X9 }
}

object QRevenantTracer
{
	frame FATB { light QIMPBAL }
	frame XXXXA { light QIMPBAL_X0 }
	frame XXXXB { light QIMPBAL_X1 }
	frame XXXXC { light QIMPBAL_X2 }
	frame XXXXD { light QIMPBAL_X3 }
	frame XXXXE { light QIMPBAL_X4 }
	frame XXXXF { light QIMPBAL_X5 }
	frame XXXXG { light QIMPBAL_X6 }
	frame XXXXH { light QIMPBAL_X7 }
	frame XXXXI { light QIMPBAL_X8 }
	frame XXXXJ { light QIMPBAL_X9 }
}

object QRevenantTracer2
{
	frame FATB { light QIMPBAL }
	frame XXXXA { light QIMPBAL_X0 }
	frame XXXXB { light QIMPBAL_X1 }
	frame XXXXC { light QIMPBAL_X2 }
	frame XXXXD { light QIMPBAL_X3 }
	frame XXXXE { light QIMPBAL_X4 }
	frame XXXXF { light QIMPBAL_X5 }
	frame XXXXG { light QIMPBAL_X6 }
	frame XXXXH { light QIMPBAL_X7 }
	frame XXXXI { light QIMPBAL_X8 }
	frame XXXXJ { light QIMPBAL_X9 }
}

pointlight QFATBAL
{
	color 0.5 0.25 0.0
	size 32
	//secondarySize 36
	//interval 1.001
}

pointlight QFATBAL_X0
{
	color 1.0 0.5 0.0
	size 40
	//secondarySize 44
	//interval 1.001
}

pointlight QFATBAL_X1
{
	color 0.9 0.45 0.0
	size 44
	//secondarySize 48
	//interval 1.001
}

pointlight QFATBAL_X2
{
	color 0.8 0.4 0.0
	size 48
	//secondarySize 52
	//interval 1.001
}

pointlight QFATBAL_X3
{
	color 0.7 0.35 0.0
	size 52
	//secondarySize 56
	//interval 1.001
}

pointlight QFATBAL_X4
{
	color 0.6 0.3 0.0
	size 56
	//secondarySize 60
	//interval 1.001
}

pointlight QFATBAL_X5
{
	color 0.5 0.25 0.0
	size 60
	//secondarySize 64
	//interval 1.001
}

pointlight QFATBAL_X6
{
	color 0.4 0.2 0.0
	size 64
	//secondarySize 68
	//interval 1.001
}

pointlight QFATBAL_X7
{
	color 0.3 0.15 0.0
	size 68
	//secondarySize 72
	//interval 1.001
}

pointlight QFATBAL_X8
{
	color 0.2 0.1 0.0
	size 72
	//secondarySize 76
	//interval 1.001
}

pointlight QFATBAL_X9
{
	color 0.1 0.05 0.0
	size 76
	//secondarySize 80
	//interval 1.001
}

object QFatShot
{
	frame MANF { light QFATBAL }
	frame XXXXA { light QFATBAL_X0 }
	frame XXXXB { light QFATBAL_X1 }
	frame XXXXC { light QFATBAL_X2 }
	frame XXXXD { light QFATBAL_X3 }
	frame XXXXE { light QFATBAL_X4 }
	frame XXXXF { light QFATBAL_X5 }
	frame XXXXG { light QFATBAL_X6 }
	frame XXXXH { light QFATBAL_X7 }
	frame XXXXI { light QFATBAL_X8 }
	frame XXXXJ { light QFATBAL_X9 }
}