Model QShotgunShell {
	Path "models\projectiles"
	Model 0 "shellcase.md2"
	Skin 0 "shellcase.png"
	Scale 2 2 2
	
	InterpolateDoubledFrames
	offset 0 0 2

	FrameIndex QSCS A 0 0
	FrameIndex QSCS B 0 1
	FrameIndex QSCS C 0 2
	FrameIndex QSCS D 0 3
	FrameIndex QSCS E 0 4
	FrameIndex QSCS F 0 5
	FrameIndex QSCS G 0 6
	FrameIndex QSCS H 0 7
	FrameIndex QSCS I 0 8
	FrameIndex QSCS J 0 9
	FrameIndex QSCS K 0 10
	FrameIndex QSCS L 0 11
}

Model QNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "spike.png"
	Scale 2 2 2

	PitchFromMomentum

	FrameIndex QSP1 A 0 0
}

Model QDeadNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "spike.png"
	Scale 2 2 2
	
	InterpolateDoubledFrames
	offset 0 0 2

	FrameIndex QSP1 A 0 0
	FrameIndex QSP1 B 0 1
	FrameIndex QSP1 C 0 2
	FrameIndex QSP1 D 0 3
	FrameIndex QSP1 E 0 4
	FrameIndex QSP1 F 0 5
	FrameIndex QSP1 G 0 6
	FrameIndex QSP1 H 0 7
	FrameIndex QSP1 I 0 8
	FrameIndex QSP1 J 0 9
	FrameIndex QSP1 K 0 10
	FrameIndex QSP1 L 0 11
}

Model QSuperNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "s_spike.png"
	Scale 2 2 2
	offset 0 0 2
	
	PitchFromMomentum
	
	FrameIndex QSP2 A 0 0
}

Model QDeadSuperNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "s_spike.png"
	Scale 2 2 2
	
	InterpolateDoubledFrames
	offset 0 0 2

	FrameIndex QSP2 A 0 0
	FrameIndex QSP2 B 0 1
	FrameIndex QSP2 C 0 2
	FrameIndex QSP2 D 0 3
	FrameIndex QSP2 E 0 4
	FrameIndex QSP2 F 0 5
	FrameIndex QSP2 G 0 6
	FrameIndex QSP2 H 0 7
	FrameIndex QSP2 I 0 8
	FrameIndex QSP2 J 0 9
	FrameIndex QSP2 K 0 10
	FrameIndex QSP2 L 0 11
}

Model QGrenade {
	Path "models\projectiles"
	Model 0 "grenade.md2"
	Skin 0 "grenade.png"
	Scale 2 2 2
	
	InterpolateDoubledFrames
	offset 0 0 8

	FrameIndex QGRN A 0 0
	FrameIndex QGRN B 0 1
	FrameIndex QGRN C 0 2
	FrameIndex QGRN D 0 3
	FrameIndex QGRN E 0 4
	FrameIndex QGRN F 0 5
	FrameIndex QGRN G 0 6
	FrameIndex QGRN H 0 7
	FrameIndex QGRN I 0 8
	FrameIndex QGRN J 0 9
	FrameIndex QGRN K 0 10
	FrameIndex QGRN L 0 11
}

Model QGrenade {
	Path "models\projectiles"
	Model 0 "grenade.md2"
	Skin 0 "grenade.png"
	Scale 2 2 2
	
	PitchFromMomentum
	InterpolateDoubledFrames
	offset 0 0 8

	FrameIndex QGRN M 0 0
}

Model QRocket {
	Path "models\projectiles"
	Model 0 "missile.md2"
	Skin 0 "missile.png"
	Scale 2 2 2
	
	PitchFromMomentum

	FrameIndex QMSL A 0 0
}

Model QLaser {
	Path "models\projectiles"
	Model 0 "laser.md2"
	Skin 0 "laser.png"
	Scale 2 2 2
	
	PitchFromMomentum
	
	FrameIndex QLAS A 0 0
}

Model QMonsterNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "spike.png"
	Scale 2 2 2

	PitchFromMomentum

	FrameIndex QSP1 A 0 0
}

Model QMonsterSuperNail {
	Path "models\projectiles"
	Model 0 "spike.md2"
	Skin 0 "s_spike.png"
	Scale 2 2 2
	offset 0 0 2
	
	PitchFromMomentum
	
	FrameIndex QSP2 A 0 0
}

Model QMonsterGrenade {
	Path "models\projectiles"
	Model 0 "grenade.md2"
	Skin 0 "grenade.png"
	Scale 2 2 2
	
	InterpolateDoubledFrames
	offset 0 0 8

	FrameIndex QGRN A 0 0
	FrameIndex QGRN B 0 1
	FrameIndex QGRN C 0 2
	FrameIndex QGRN D 0 3
	FrameIndex QGRN E 0 4
	FrameIndex QGRN F 0 5
	FrameIndex QGRN G 0 6
	FrameIndex QGRN H 0 7
	FrameIndex QGRN I 0 8
	FrameIndex QGRN J 0 9
	FrameIndex QGRN K 0 10
	FrameIndex QGRN L 0 11
}

Model QMonsterGrenade {
	Path "models\projectiles"
	Model 0 "grenade.md2"
	Skin 0 "grenade.png"
	Scale 2 2 2
	
	PitchFromMomentum
	InterpolateDoubledFrames
	offset 0 0 8

	FrameIndex QGRN M 0 0
}

Model QMonsterRocket {
	Path "models\projectiles"
	Model 0 "missile.md2"
	Skin 0 "missile.png"
	Scale 2 2 2
	
	PitchFromMomentum

	FrameIndex QMSL A 0 0
}

Model QMonsterLaser {
	Path "models\projectiles"
	Model 0 "laser.md2"
	Skin 0 "laser.png"
	Scale 2 2 2
	
	PitchFromMomentum
	
	FrameIndex QLAS A 0 0
}