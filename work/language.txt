[enu default]

// Quit Doom 1 messages
QUITMSG1 = "are you sure you want to\nquit this great game?";
QUITMSG2 = "are you sure you want to\nquit this great game?";
QUITMSG3 = "are you sure you want to\nquit this great game?";
QUITMSG4 = "are you sure you want to\nquit this great game?";
QUITMSG5 = "are you sure you want to\nquit this great game?";
QUITMSG6 = "are you sure you want to\nquit this great game?";
QUITMSG7 = "are you sure you want to\nquit this great game?";

// Quit Doom II messages
QUITMSG8 = "are you sure you want to\nquit this great game?";
QUITMSG9 = "are you sure you want to\nquit this great game?";
QUITMSG10 = "are you sure you want to\nquit this great game?";
QUITMSG11 = "are you sure you want to\nquit this great game?";
QUITMSG12 = "are you sure you want to\nquit this great game?";
QUITMSG13 = "are you sure you want to\nquit this great game?";
QUITMSG14 = "are you sure you want to\nquit this great game?";

// Quit Strife messages
QUITMSG15 = "are you sure you want to\nquit this great game?";
QUITMSG16 = "are you sure you want to\nquit this great game?";
QUITMSG17 = "are you sure you want to\nquit this great game?";
QUITMSG18 = "are you sure you want to\nquit this great game?";
QUITMSG19 = "are you sure you want to\nquit this great game?";
QUITMSG20 = "are you sure you want to\nquit this great game?";
QUITMSG21 = "are you sure you want to\nquit this great game?";
QUITMSG22 = "are you sure you want to\nquit this great game?";

// Quit Chex messages
QUITMSG23 = "are you sure you want to\nquit this great game?";
QUITMSG24 = "are you sure you want to\nquit this great game?";
QUITMSG25 = "are you sure you want to\nquit this great game?";
QUITMSG26 = "are you sure you want to\nquit this great game?";
QUITMSG27 = "are you sure you want to\nquit this great game?";
QUITMSG28 = "are you sure you want to\nquit this great game?";
QUITMSG29 = "are you sure you want to\nquit this great game?";