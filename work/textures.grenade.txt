Sprite "QGRNE1", 64, 64
{
	Offset 32, 36
	Patch "QGRNC5", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF1", 64, 64
{
	Offset 32, 36
	Patch "QGRNB5", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG1", 64, 64
{
	Offset 32, 36
	Patch "QGRNA5", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH1", 64, 64
{
	Offset 32, 36
	Patch "QGRNB5", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI1", 64, 64
{
	Offset 32, 36
	Patch "QGRNC5", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ1", 64, 64
{
	Offset 32, 36
	Patch "QGRND5", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK1", 64, 64
{
	Offset 32, 36
	Patch "QGRNC1", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL1", 64, 64
{
	Offset 32, 36
	Patch "QGRNB1", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM1", 64, 64
{
	Offset 32, 36
	Patch "QGRNA1", 0, 0
}

Sprite "QGRNE2E8", 64, 64
{
	Offset 32, 36
	Patch "QGRNC4C6", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF2F8", 64, 64
{
	Offset 32, 36
	Patch "QGRNB4B6", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG2G8", 64, 64
{
	Offset 32, 36
	Patch "QGRNA4A6", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH2H8", 64, 64
{
	Offset 32, 36
	Patch "QGRNB4B6", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI2I8", 64, 64
{
	Offset 32, 36
	Patch "QGRNC4C6", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ2J8", 64, 64
{
	Offset 32, 36
	Patch "QGRND4D6", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK2K8", 64, 64
{
	Offset 32, 36
	Patch "QGRNC2C8", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL2L8", 64, 64
{
	Offset 32, 36
	Patch "QGRNB2B8", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM2M8", 64, 64
{
	Offset 32, 36
	Patch "QGRNA2A8", 0, 0
}

Sprite "QGRNE3E7", 64, 64
{
	Offset 32, 36
	Patch "QGRNC3C7", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF3F7", 64, 64
{
	Offset 32, 36
	Patch "QGRNB3B7", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG3G7", 64, 64
{
	Offset 32, 36
	Patch "QGRNA3A7", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH3H7", 64, 64
{
	Offset 32, 36
	Patch "QGRNB3B7", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI3I7", 64, 64
{
	Offset 32, 36
	Patch "QGRNC3C7", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ3J7", 64, 64
{
	Offset 32, 36
	Patch "QGRND3D7", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK3K7", 64, 64
{
	Offset 32, 36
	Patch "QGRNC3C7", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL3L7", 64, 64
{
	Offset 32, 36
	Patch "QGRNB3B7", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM3M7", 64, 64
{
	Offset 32, 36
	Patch "QGRNA3A7", 0, 0
}

Sprite "QGRNE4E6", 64, 64
{
	Offset 32, 36
	Patch "QGRNC2C8", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF4F6", 64, 64
{
	Offset 32, 36
	Patch "QGRNB2B8", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG4G6", 64, 64
{
	Offset 32, 36
	Patch "QGRNA2A8", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH4H6", 64, 64
{
	Offset 32, 36
	Patch "QGRNB2B8", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI4I6", 64, 64
{
	Offset 32, 36
	Patch "QGRNC2C8", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ4J6", 64, 64
{
	Offset 32, 36
	Patch "QGRND2D8", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK4K6", 64, 64
{
	Offset 32, 36
	Patch "QGRNC4C6", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL4L6", 64, 64
{
	Offset 32, 36
	Patch "QGRNB4B6", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM4M6", 64, 64
{
	Offset 32, 36
	Patch "QGRNA4A6", 0, 0
}

Sprite "QGRNE5", 64, 64
{
	Offset 32, 36
	Patch "QGRNC1", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF5", 64, 64
{
	Offset 32, 36
	Patch "QGRNB1", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG5", 64, 64
{
	Offset 32, 36
	Patch "QGRNA1", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH5", 64, 64
{
	Offset 32, 36
	Patch "QGRNB1", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI5", 64, 64
{
	Offset 32, 36
	Patch "QGRNC1", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ5", 64, 64
{
	Offset 32, 36
	Patch "QGRND1", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK5", 64, 64
{
	Offset 32, 36
	Patch "QGRNC5", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL5", 64, 64
{
	Offset 32, 36
	Patch "QGRNB5", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM5", 64, 64
{
	Offset 32, 36
	Patch "QGRNA5", 0, 0
}

Sprite "QGRNE9EG", 64, 64
{
	Offset 32, 36
	Patch "QGRNCCCD", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNF9FG", 64, 64
{
	Offset 32, 36
	Patch "QGRNBCBD", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNG9GG", 64, 64
{
	Offset 32, 36
	Patch "QGRNACAD", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNH9HG", 64, 64
{
	Offset 32, 36
	Patch "QGRNBCBD", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNI9IG", 64, 64
{
	Offset 32, 36
	Patch "QGRNCCCD", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJ9JG", 64, 64
{
	Offset 32, 36
	Patch "QGRNDCDD", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNK9KG", 64, 64
{
	Offset 32, 36
	Patch "QGRNC9CG", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNL9LG", 64, 64
{
	Offset 32, 36
	Patch "QGRNB9BG", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNM9MG", 64, 64
{
	Offset 32, 36
	Patch "QGRNA9AG", 0, 0
}

Sprite "QGRNEAEF", 64, 64
{
	Offset 32, 36
	Patch "QGRNCBCE", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNFAFF", 64, 64
{
	Offset 32, 36
	Patch "QGRNBBBE", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNGAGF", 64, 64
{
	Offset 32, 36
	Patch "QGRNABAE", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNHAHF", 64, 64
{
	Offset 32, 36
	Patch "QGRNBBBE", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNIAIF", 64, 64
{
	Offset 32, 36
	Patch "QGRNCBCE", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJAJF", 64, 64
{
	Offset 32, 36
	Patch "QGRNDBDE", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNKAKF", 64, 64
{
	Offset 32, 36
	Patch "QGRNCACF", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNLALF", 64, 64
{
	Offset 32, 36
	Patch "QGRNBABF", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNMAMF", 64, 64
{
	Offset 32, 36
	Patch "QGRNAAAF", 0, 0
}

Sprite "QGRNEBEE", 64, 64
{
	Offset 32, 36
	Patch "QGRNCACF", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNFBFE", 64, 64
{
	Offset 32, 36
	Patch "QGRNBABF", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNGBGE", 64, 64
{
	Offset 32, 36
	Patch "QGRNAAAF", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNHBHE", 64, 64
{
	Offset 32, 36
	Patch "QGRNBABF", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNIBIE", 64, 64
{
	Offset 32, 36
	Patch "QGRNCACF", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJBJE", 64, 64
{
	Offset 32, 36
	Patch "QGRNDADF", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNKBKE", 64, 64
{
	Offset 32, 36
	Patch "QGRNCBCE", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNLBLE", 64, 64
{
	Offset 32, 36
	Patch "QGRNBBBE", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNMBME", 64, 64
{
	Offset 32, 36
	Patch "QGRNABAE", 0, 0
}

Sprite "QGRNECED", 64, 64
{
	Offset 32, 36
	Patch "QGRNC9CG", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNFCFD", 64, 64
{
	Offset 32, 36
	Patch "QGRNB9BG", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNGCGD", 64, 64
{
	Offset 32, 36
	Patch "QGRNA9AG", 0, 0
	{
		FlipX
	}
}

Sprite "QGRNHCHD", 64, 64
{
	Offset 32, 36
	Patch "QGRNB9BG", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNICID", 64, 64
{
	Offset 32, 36
	Patch "QGRNC9CG", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNJCJD", 64, 64
{
	Offset 32, 36
	Patch "QGRND9DG", 0, 0
	{
		FlipX
		FlipY
	}
}

Sprite "QGRNKCKD", 64, 64
{
	Offset 32, 36
	Patch "QGRNCCCD", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNLCLD", 64, 64
{
	Offset 32, 36
	Patch "QGRNBCBD", 0, 0
	{
		FlipY
	}
}

Sprite "QGRNMCMD", 64, 64
{
	Offset 32, 36
	Patch "QGRNACAD", 0, 0
}